import React, {Component} from 'react';

export class Counter extends Component {
  render() {
    return (
      <div>
        <h1 className="title">This is my Counter</h1>
        <h2 className="subtitle">It uses Redux</h2>
        <div className="counter">
          <button onClick={this.props.decrement} className="button is-primary">-</button>
          <input type="text" value={this.props.counter} className='input'/>
          <button onClick={this.props.increment} className="button is-primary">+</button>
        </div>
      </div>
    )
  }
}