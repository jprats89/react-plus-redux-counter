import React from 'react';
import CounterContainer from './containers/Counter';
import {counterApp} from './reducers';
import {Provider} from 'react-redux';
import {createStore} from  'redux';

import './App.css';
import 'bulma/css/bulma.css'

const store = createStore(counterApp);

function App() {
  return (
    <Provider store={store}>
        <CounterContainer />
    </Provider>
  );
}

export default App;
