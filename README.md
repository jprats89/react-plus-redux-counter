#### Description
It is a counter with 2 buttons Increment and Decrement starting in 0.<br>
This project was created following a [React course](https://www.udemy.com/course/aprendiendo-react/) in order to learn basic concepts. 
<b>Redux</b> was used to create this counter

#### Demo site
https://jprats89.gitlab.io/react-plus-redux-counter/

#### Run the project
To check the project just run from the root:

##### `npm start`

